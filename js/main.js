(function(a){a.expr[":"].onScreen=function(b){var c=a(window),d=c.scrollTop(),e=c.height(),f=d+e,g=a(b),h=g.offset().top,i=g.height(),j=h+i;return h>=d&&h<f||j>d&&j<=f||i>e&&h<=d&&j>=f}})(jQuery);


/*Modals form*/
var __opts = {popup:{}}
__opts.popup.defaults = {
  tClose: 'Закрыть(Esc)',
  tLoading: 'Загрузка...',
  type: 'inline',
  fixedContentPos: true,
  fixedBgPos: true,
  image: {
    tError: 'Произошла ошибка при загрузке <a href="%url%">изображения</a>.'
  },
  ajax: {
    settings: null,
    cursor: 'isProcessed',
    tError: 'Ошибка при загрузке <a href="%url%">содержимого</a>'
  },
  callbacks: {
    beforeOpen: function() {
      $('body').addClass('withModalOpen');
    },
    open: function() {
      $('input[type="tel"],input.phone',this.content).mask('+7 (999) 999-99-99');
    },
    change: function() {
      $('input[type="tel"],input.phone',this.content).mask('+7 (999) 999-99-99');
    },
    ajaxContentAdded: function() {
      $('input[type="tel"],input.phone',this.content).mask('+7 (999) 999-99-99');
    },
    afterClose: function() {
      $('body').removeClass('withModalOpen');
    },
  },

  verticalFit: true,
  closeBtnInside: true,
  preloader: true,
  removalDelay: 500,
  mainClass: 'mfp-slide-bottom'
};
__opts.popup.gallery = $.extend(true,{},__opts.popup.defaults,{
		gallery: {
			enabled: true,
			tPrev: 'Предыдущее (&larr;)',
			tNext: 'Следующее (&rarr;)',
			tCounter: '%curr% из %total%',
			preload: [1,2],
		},
		modal:false,
		type: 'image',
});




$(function(){ // Ready function ===>

  $(".item.sub").on("click",".switch", function () {
    owner = $(this).parent(); // li
    if (owner.hasClass("open")) {
      owner.removeClass("open");
    } else {
      owner.parent().children().removeClass("open");
      owner.toggleClass("open");
    }
  });
$(".tab-box .tab-href .tab-href-list").on("click", ".item" ,function(e) {
	if ($(this).hasClass("active")) {
		  return;
	} else {
	$(this).parents().children().removeClass("active");
	$(this).addClass("active");
	dataHrefOpen = $(this).attr("data-href");
	$(".tab-content .tab-list .item").removeClass("vizible");
	$(".tab-content .tab-list .item[data-href-open="+dataHrefOpen+"]").addClass("vizible");
	}
});

$(".DropDownBox").on("click", ".title" ,function(e) {
	$(this).parent().toggleClass("open");
});

/*Клик на фото вызов галереи*/
$('.gallery-list').on('click','.item a',function(e){
	if ($.fn.magnificPopup)
	{
		var owner = $(this).parents('.slider-list').last();
		if (!owner.__items)
		{
			owner.__items = [];
			$('.item a',owner).each(function(){
				owner.__items.push({src:this.href,type:'image'});
			})
		}
		$.magnificPopup.open($.extend(true,{},__opts.popup.gallery,{items: owner.__items}),$(this.parentNode).index());

		return false;
	}
});

$(".sort-button-list").on("click",".item:not(.active)", function () {
  if ( $(this).children().is(".icon-sort-table") ) {
    $(".auto-list").addClass("tableVid");
    $(this).parent().children().removeClass("active");
    $(this).addClass("active");
  }
  if ( $(this).children().is(".icon-sort-kub") ) {
    $(".auto-list").removeClass("tableVid");
    $(this).parent().children().removeClass("active");
    $(this).addClass("active")
  }
});
/*Развернуть свернуть*/
$('.hidden-box').not('btn-vizible-hidden-block').after("<div class='btn-vizible-hidden-block'><span></span></div>");
$('.btn-vizible-hidden-block').click(function() {
    $('.btn-vizible-hidden-block').toggleClass('vizible');
    $('.hidden-box').toggleClass('vizible');
});

/*бутер open*/
$(".nav-btn").click(function () {
  $("body").toggleClass("active-menu");
});

/*города open*/
$(".city:not(.city-one) .city-selection .city-name,.city:not(.city-one) .city-target a").click(function() {
	$(this).parents(".city").toggleClass("open");
});

/*Сортировка open*/
$(".select-box .sort-choice,.sort-select a").click(function() {
	$(this).parents(".selection").toggleClass("open");
});

/* Акции новости */
$(".tab").click(function() {
$(this).parents().children().removeClass("active");
$(this).addClass("active");
var dataVizBlock = $(this).attr("data-block-vizible");
var $wrap = $(this).parents(".wrapper");
$wrap.children().removeClass("vizible");
$wrap.children("."+dataVizBlock).addClass("vizible");
})

$(".tab").click(function() {
$(this).parents().children().removeClass("active");
$(this).addClass("active");
var dataVizBlock = $(this).attr("data-block-vizible");
var $wrap = $(this).parents(".wrapper");
$wrap.children(".brends-list-box").removeClass("vizible");
$wrap.children(".brends-list-box[data-list-tab="+dataVizBlock+"]").addClass("vizible");
});



//Install mask
if ($.fn.mask)
{
	$(document).on('focus','input[type="tel"],input.phone',function(){
		$(this).mask('+7 (999) 999-99-99');
	})
}



$('.navigationBlock .navigationWrapper .nav-button').on("click", function() {
 $(this).parentsUntil(".navigationBlock").last().parent().toggleClass('open');
});

$('.question-list .icon-plus').on("click", function() {
 if ($(this).parent().parent().hasClass("item-active")) {
	 $(this).parentsUntil(".item").last().parent().toggleClass('item-active');
 }
 else {
 $('.question-list .item').removeClass('item-active');
 $(this).parentsUntil(".item").last().parent().addClass('item-active');
 }
});



/*Мелкие слайдеры*/

$('.slider-list').each(function (index){
console.log(index);
$(this).wrap($("<div class='catalogScrollBlock'><div class='scrollWrapper'></div></div>"));
var owner=this.parentNode.parentNode;
console.log(owner);
$(owner).prepend($("<div class='scrollNavBlock'><a href='#' class='step step-prev'></a><a href='#' class='step step-next'></a></div><div class='navBlock navBlock-slides'></div>"));
var $list = $(this);
var $outer = $list.parent();

$('.item:first-child',$list).addClass('item-active');

owner.__repos = function(auto)
  {
		if (!this.__offset) {this.__offset=0;}


	 var listWidth = $('.item:last-child',$list);
   listWidth = listWidth.outerWidth(true)+listWidth.position().left;

	 var steps = [];
   console.log(listWidth,$outer.outerWidth());
	 steps.length = Math.ceil(listWidth/$outer.outerWidth());

	 $('.navBlock-slides',owner).html('<a class="slide" href="#">'+steps.join('</a> <a class="slide" href="#">')+'</a>');

   if (auto==true )
   {
		 var diff = owner.__offset/$outer.width();
		 var diffCount = Math.floor(diff);

		 owner.__offset = $outer.width()*Math.round(diff-diffCount);
  }

	 $('.scrollNavBlock .step',owner).removeClass('step-inactive');
	 $('.navBlock-slides .slide',owner).removeClass('slide-active');

	 if ($outer.width()<listWidth)
   {
		 $(owner).addClass('isOverflown');

		$('.scrollNavBlock .step',owner).removeClass('step-inactive');
    if (owner.__offset<=0)
    {
     owner.__offset = 0;
     $('.scrollNavBlock .step-prev',owner).addClass('step-inactive');
	 	}
		if (owner.__offset+$outer.width()>=listWidth)
    {
     owner.__offset = listWidth - $outer.width() ;
     $('.scrollNavBlock .step-next',owner).addClass('step-inactive');
    }
	}
   else
   {
		$(owner).removeClass('isOverflown');
		this.__offset = 0;
    $('.scrollNavBlock .step',owner).addClass('step-inactive');
   }
	 this.__offset=Math.round(this.__offset);
	 var idx = Math.floor(this.__offset/$outer.width());

	 console.log(idx,this.__offset,$outer.width(),this.__offset/$outer.width());

	 $('.navBlock-slides .slide:eq('+idx+')',owner).addClass('slide-active');

   $list.css('left',-this.__offset);
  }
owner.__repos(true);

$(owner).on('click','.navBlock-slides .slide',function(){
	var idx = $(this).index();
	owner.__offset = idx * $outer.width();
	owner.__repos(true);
	return false;


})

$(owner).on('click','.scrollNavBlock .step',function(){
var direction = $(this).hasClass('step-next') ? 1 : -1;
//console.log(direction);
if ($(owner).hasClass('imageGalleryBlock-compact'))
{
  var idx = $('.item-active',$list).index() + direction;
  if (idx<0)
  {
    idx=$('.item',$list).length-1;
  }
  if (idx>$('.item',$list).length-1)
  {
    idx=0;
  }
  $('.item',$list).removeClass('item-active').eq(idx).find('>a').click();
}
else
{
  owner.__offset = owner.__offset + direction * $outer.width();
}
owner.__repos();
return false;
})

$('.item:first-child a',$list).click();

});


$('body').on('click','[data-modal-form]',function(e) {
  e.preventDefault();
  var owner = this;
  var blockName = $(this).data('modal-form');
  var modalAction = $(this).data('modal-action');
  var options = {items: {src:this.href},type:'ajax',
    callbacks: {
      parseAjax: function(response)
      {
        var html = $('<div>'+response.data+'</div>').find('.'+blockName);
        html = html.length ? html.html() : "<div class='message message-error'>Ошибка при загрузке</div>";
        response.data = "<div class='popupBlock popupBlock-modal b-blue b-form' data-block-name='"+blockName+"' data-modal-action='"+(modalAction?modalAction:'')+"'><div class='modalBlock "+blockName+"'>"+html+"</div></div>";
      }
    }
  }
  $.magnificPopup.open($.extend(true,{},__opts.popup.defaults,options));
  return false;
})


if (XMLHttpRequest && FormData)
{
  $('body').on('submit','.popupBlock form',function(e){
    e.preventDefault();

    $('[type="submit"]',this).attr('disabled','disabled');

    $(this).addClass('form-processing');

    var $ownerBlock = $(this).parentsUntil('.popupBlock').last().addClass('modalBlock');
    var modalAction = $ownerBlock.parent().data('modal-action');
    var owner = this;

    var formData = new FormData(this);
    formData.append('ajax', 1);
    var val = $(this).find("input[type=submit],button");
    formData.append(val[0].name, val[0].value);

    var xhr = new XMLHttpRequest();
    xhr.open(this.method, this.action, true);

    xhr.onreadystatechange = function() {
      var msg;
      if (xhr.readyState == 4)
      {
        var html='';
        if(xhr.status == 200)
        {
          var $response = $(xhr.responseText);
          var $block = $response.find('.'+$ownerBlock.parent().data('block-name'));
          if (!$block.length && $response.hasClass($ownerBlock.parent().data('block-name'))) {
            $block = $response;
          }
          var html = $block.html();
          if (!$block.length || $block.find('>.message-success').length)
          {
            if (modalAction=='load')
            {
              html = false;
              $.magnificPopup.close();
              window.location=owner.action;
            }

            if (modalAction=='refresh')
            {
              html = false;
              $.magnificPopup.close();
              window.location.reload(true);
            }
          }
          if (html)
          {
            $ownerBlock.html(html);
          }
          $('input[type="tel"],input.phone',$ownerBlock).mask('+7 (999) 999-99-99');
          labelClearing($ownerBlock);
        }
        else
        {
          $ownerBlock.html("<div class='message message-error'>Возникла ошибка при отправке данных</div>");
        }


      }
    };

    xhr.send(formData);
    return false;
  })
}
/*modals end*/



$(window).resize(function(){

	$('.slider-list').each(function(){
		this.parentNode.parentNode.__repos(true);
	});
})



/*Галерея*/

$('.imageGalleryBlock').each(function() {
    var owner = this;
    $(this).prepend($("<div class='navBlock navBlock-steps'><a href='#' class='step step-prev'>Назад</a><a href='#' class='step step-next'>Вперёд</a></div><div class='previewBlock'><div class='imageHolder'><div class='imageWrapper'><div class='box-slide-image'><img src='' class='image' /></div></div></div></div>"));
    var $list = $('.imageList', this);
    var $outer = $list.parent();
    this.__repos = function(auto) {

				var listWidth = 0;
				$('.item',$list).each(function(){
					listWidth = Math.max($(this).outerWidth(true)+$(this).position().left,listWidth);
				});

        if (auto == true) {
            owner.__offset = -($outer.width() / 2 - $('.item-active', $list).offset().left - $('.item-active', $list).width() / 2 + $list.offset().left);
        }
        if ($outer.width() <= $list.width()) {
            $('.navBlock .step', owner).removeClass('step-inactive');
            if (owner.__offset < 0) {
                owner.__offset = 0;
                $('.navBlock .step-prev', owner).addClass('step-inactive');
            }
            if (owner.__offset + $outer.width() > listWidth) {
                owner.__offset = listWidth - $outer.width();
                $('.navBlock .step-next', owner).addClass('step-inactive');
            }
        } else {
            owner.__offset = 0;
            $('.navBlock .step', owner).addClass('step-inactive');
        }
        $list.css('left', -this.__offset);
    }
    $(this).on('click', '.navBlock .step', function() {
        var direction = $(this).hasClass('step-next') ? 1 : -1;
        console.log(direction);
        if ($(owner).hasClass('imageGalleryBlock-compact')) {
            var idx = $('.item-active', $list).index() + direction;
            if (idx < 0) {
                idx = $('.item', $list).length - 1;
            }
            if (idx > $('.item', $list).length - 1) {
                idx = 0;
            }
            $('.item', $list).removeClass('item-active').eq(idx).find('>a').click();
        } else {
            owner.__offset = owner.__offset + direction * $outer.width();
        }
        owner.__repos();
        return false;
    })
    $list.on('click', '.item a', function() {
        var url = this.href;
        $('.previewBlock', owner).addClass('previewBlock-loading');
        $('.previewBlock .image', owner).fadeOut(500, function() {
            var img = new Image;
            img.onload = function() {
                $('.previewBlock', owner).removeClass('previewBlock-loading');
                $('.previewBlock .image', owner).attr('src', this.src).fadeIn(500);
            }
            img.src = url;
        });
        $('.item', $list).removeClass('item-active');
        $(this.parentNode).addClass('item-active');
        owner.__repos(true);
        return false;
    })
    $('.item:first-child a', $list).click();
});

$('.steppedForm').each(function(){
	var form = this;

	var $fieldsets = $('fieldset',form);

	$fieldsets.attr('disabled',true);
	$fieldsets.eq(0).removeAttr('disabled').addClass('step-current');

	$(this).submit(function(e){

		if (!(typeof document.createElement('input').checkValidity === 'function'))
		{
			e.preventDefault();

			$('[required]',form).each(function(){
				console.log(this,this.value);
				if (!this.value)
				{
					this.checkValidity();
					return false;
				}
			})

			$('[pattern]',form).each(function(){
				var regexp = new RegExp(this.getAttribute('pattern'),'i');

				console.log(regexp);

				if (!(regexp.test(this.value)))
				{
					this.focus();
					return false;
				}
			})
		}

		var idx = $fieldsets.index($('fieldset.step-current'));

		$fieldsets.eq(idx).attr('data-fieldset-validated',1);

		if($fieldsets.length==$('[data-fieldset-validated="1"]').length)
		{

		}
		else
		{
			$fieldsets.eq(idx).data('fieldset-validated',1);
			$fieldsets.attr('disabled',true).removeClass('step-current').eq((idx+1>$fieldsets.length)?idx:idx+1).removeAttr('disabled').addClass('step-current');

			return false;

		}



		alert('sent!');

		return false;
	})
})



//
$(window).scroll(function(){
		var sT = $(window).scrollTop();
		var nH = $('header .navigationBlock').height();
		var wH = $(window).height();

		//$('header .decalBlock .decal').css('top',sT*.08);

		var threshold = $('header').outerHeight(true)-(wH-nH)/2;
		$('body')[sT>threshold?'addClass':'removeClass']('isOverscrolled')[sT<=threshold?'addClass':'removeClass']('isNotOverscrolled');

		$('.advantages, .question .form-application ,.form-application, .plus-fransize, .review , .gallery , .specifications-block .text-wrap ol, .question').each(function(){
			if ($(this).is(':onScreen'))
			{
				$(this).addClass('isInView');
			}
		});

		$('.navigationBlock .menuItemList a[href*="#"]').each(function(){
			var hash = this.href.split('#')[1];
			var el = hash?$('#'+hash):false;
			if (hash && el && el.length && $(el).is(':onScreen'))
			{
				$('.navigationBlock .menuItemList a').parent().removeClass('item-active');
				$(this.parentNode).addClass('item-active');
			}
		})

	}).scroll();


	$(window).resize(function(){
		var nH = $('header .navigationBlock').height();
		var wH = $(window).height();
		if (nH>wH)
		{
			$('header .navigationBlock').removeClass('navigationBlock-fit').addClass('navigationBlock-overgrow').css('top',0);
		}
		else
		{
			$('header .navigationBlock').addClass('navigationBlock-fit').removeClass('navigationBlock-overgrow').css('top',(wH-nH)/2);
		}

		$('.scrollBlock .imageList,.scrollBlock .reviewList').each(function(){
			$(this.parentNode.parentNode)[($(this).width() > $(this.parentNode).width())?'addClass':'removeClass']('scrollBlock-overflow');
		})
	}).resize();
});


$('html').removeClass('jsLoading').addClass('jsLoaded');
